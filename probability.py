#-*- coding: utf-8 -*-
# Author : Daudré--Treuil Prunelle & Masse Océane
# Avril 2020

##################################################### IMPORTS :

import copy
from collections import Counter

from reading import *  # module, part1 

##################################################### FUNCTIONS :

def p_pos_neg(path):
    """
    Function that returns probabilities P(POS) and P(NEG) in tweets_train
    in: str
    out: float, float
    """
    num_pos = len(read_tweets_known(path)[0])
    num_neg = len(read_tweets_known(path)[1])
    p_pos = num_pos / (num_pos + num_neg)
    p_neg = num_neg / (num_pos + num_neg) # i could have done 1 - p_pos, idk, tell me which one is better
    return p_pos, p_neg


def words_corpus(path): # when you don't know the labels
    """
    Function that returns the total words number, and the list of them
    in: str
    out: int, list
    """
    l_clean_tweets = clean_all_tweets(read_tweets_unknow(path)[0])
    l_word=[]
    for tweet in l_clean_tweets:
        l_word += tweet
    n_corp = len(l_word)
    return n_corp, l_word


def count_words(l_word):
    """
    Function that returns a dict with each word and his occurence
    in: list
    out: dict
    """
    return dict(Counter(l_word)) # dict with the occurence of each word in the list l_word


def p_word(d_occur, n_corp):
    """
    Function that returns a dict with each word in d_occur and their proba.
    n_corp is the total number in the corpus
    in: dict, int
    out: dict
    """
    dict_w_p = copy.deepcopy(d_occur) # dict_w_p stands for dict_word_proba
    for w in dict_w_p:
        dict_w_p[w] = dict_w_p.get(w,0) / n_corp
    return dict_w_p

def words_pos_neg(path): # when you know the labels
    """
    functions that returns the total words number of pos and neg, and the list
    of them
    in: str
    out: int, int, list, list
    """
    l_tweets_pos, l_tweets_neg = read_tweets_known(path)
    l_clean_tweets_pos = clean_all_tweets(l_tweets_pos) # list which contains the list of words for each tweet
    l_clean_tweets_neg = clean_all_tweets(l_tweets_neg)
    l_word_pos = sum(l_clean_tweets_pos, []) # give a list of all word in one list instead of as many lists as tweets
    l_word_neg = sum(l_clean_tweets_neg, [])
    return len(l_word_pos), len(l_word_neg), l_word_pos, l_word_neg
