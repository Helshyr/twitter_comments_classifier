#-*- coding: utf-8 -*-
# Author : Daudré--Treuil Prunelle & Masse Océane
# Avril 2020

############################################### IMPORTS :

import pandas as pd

############################################### FUNCTIONS :

def read_tweets_known(path):
    """
    Read a csv of tweets and return two lists, one with all the positive tweets
    (l_positive) and one with all the negative tweets (l_negative)

    in: str
    out: tuple (<class 'pandas.core.series.Series'>)
    """
    df = pd.read_csv(path, sep=",", header=None, skipinitialspace = True, quotechar = '"')
    a_positive = df[df[0]=="positive"]
    a_negative = df[df[0]=="negative"]
    l_positive = a_positive[1]
    l_negative = a_negative[1]
    return l_positive, l_negative

def read_tweets_unknow(path):
    """
    Read a csv of tweets and return two lists, one with all the tweets
    (l_tweets) and one with all the labeL (l_label)

    in: str
    out: tuple (<class 'pandas.core.series.Series'>)
    """
    df = pd.read_csv(path, sep=",", header=None, skipinitialspace = True, quotechar = '"')
    l_tweets = df[1]
    l_label = df[0]
    return l_tweets, l_label

def clean_tweets(twt):
    """
    Take a string and return a list of all its words after a pre-treatment :
    - clean all :,()[]'?!;.-{}/'\'\"'
    - delete all words shorter than 4
    - keep the smileys ;)

    in: str
    out: list
    """
    supr = "=():$£€;*,[]'?!-{}/'\'\"'0123456789"
    smiley = [':)', ':(', ':D', ':/', ':p', ':P', '=)', '=(', '=D', '=/', '=p', '=P',  ';)', ';(', ';D', ';/', ';p', ';P', '<3', 'RT']
    twt = twt.replace(".", " ")
    l_twt = twt.split(None)
    l2_twt = []
    for elem in l_twt:
        if (len(elem) > 3):
            elem = elem.lower()
            for x in supr:
                elem = elem.replace(x, "")
            l2_twt.append(elem)
        elif (elem in smiley):
            l2_twt.append(elem)
    return l2_twt

def clean_all_tweets(l_tweets):
    """
    Take a list of tweets and clean each tweets with clean_tweets

    in: list
    out: list ( with each words (str) of the tweet)
    """
    l_tweets_clean = []
    for elem in l_tweets:
        temp = clean_tweets(elem)
        if (len(temp) > 1):
            l_tweets_clean.append(temp)
    return l_tweets_clean
