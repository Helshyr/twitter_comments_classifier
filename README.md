# Comment Classifier

This project was done as part of the course "Traitement automatique de corpus" of the University of Lille with M.Liétard. The goal is to classify some tweets send to big american airline compagnies into two categories : "positive" and "negative", using a naive bayesian classifier.

## Organisation :

The project is composed of three sets :

* tweets_dev.csv that we used for our tests during the development phase
* tweets_test.csv that we used for our test during the demonstration
* tweets_train.csv that we used for our training

And three python files :

* reading.py who open our sets and clean the tweets
* probability.py who deal with all the training
* classification.py who is our main file

## Command line usage :

To launch our project, the following command must be used :

```
python3 classification.py
```

## Team :

This project is a work by Océane Masse and Prunelle Daudré--Treuil, students in Mathematics and Computer Science applied to Humans and Socials Sciences
